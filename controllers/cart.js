const Cart = require("../models/Cart")
exports.createCart = async (req, res) => {
    let cart;
    cart = await Cart.findOne({ user: req.user._id });



    if (cart) {
        //add or push cart items if product(productId) change but 
        //  only update the quantity if any product stays the same

        const product = await req.body.cartItems.product
        const itemThatExists = await cart.cartItems.find(c => c.product === product)

        if (itemThatExists) {

            cart = await Cart.findOneAndUpdate({ "user": req.user._id, "cartItems.product": product },
                {
                    "$set": {
                        "cartItems.$": {
                            ...req.body.cartItems,
                            quantity: itemThatExists.quantity + req.body.cartItems.quantity
                        }

                    }
                }
            )
            // res.status(200).json({cart})
        } else {
            cart = await Cart.findOneAndUpdate({ user: req.user._id },
                { "$push": { "cartItems": req.body.cartItems } });

            // res.status(200).json({cart})
        }

        res.status(200).json({ cart })


    } else {
        cart = await Cart.create({
            user: req.user._id,
            cartItems: [req.body.cartItems]
        })
        res.status(201).json({ cart })
    }


}