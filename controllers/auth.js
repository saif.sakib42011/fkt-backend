const User =require("../models/User");
const shortid=require("shortid")

exports.register=async(req,res)=>{
 try {
    const {firstname,lastname,email,password}=req.body;
    const user=await  User.create({
        firstname,
        lastname,
        email,
        password,
        username:shortid.generate()});
    
    res.status(201).json({success:true,user})
 } catch (error) {
    res.status(500).json({success:false,error:"user email already exists"})     
 }

}


exports.login=async(req,res)=>{
    try {
        const {email,password}=req.body;
        const user=await User.findOne({email}).select("+password");
        if (!user) {
            res.status(401).json({success:"false",error:"invalid credentials"}); 
        }
        
        const isMatch=await user.matchPassword(password);
        if (!isMatch) {
            res.status(404).json({success:"false",error:"please provide valid email and password"});
        }

        signedinToken(200,res,user);

    } catch (error) {
        res.status(401).json({success:"false",error:"invalid credentials"}); 
    }
}

const signedinToken=(statusCode,res,user)=>{
    const token=user.getSignedToken();
    return res.status(statusCode).json(
        { token,
          user:{_id:user._id,firstname:user.firstname,lastname:user.lastname,email:user.email,role:user.role,fullname:`${user.firstname} ${user.lastname}`}     
        })
}




