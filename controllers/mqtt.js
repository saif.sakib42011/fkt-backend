const Mqtt = require('../models/Mqtt');
const slugify = require("slugify")
const shortid = require("shortid")

exports.createMqtt = async (req, res) => {
    const { name, user } = req.body;
    let mqttImage;
        if (req.file) {
            mqttImage = process.env.API + "/public/" + req.file.filename
        }
    const mqttdata = await Mqtt.create({
        name,
        slug: `${slugify(name)}-${shortid.generate()}`,
        parentId: req.body.parentId ? req.body.parentId : undefined,
        user,
        mqttImage
        // button: req.body.button,
    })
    res.status(201).json({ mqttdata })

}

function addPubData(names, parentId = null) {
    let name;
    if (parentId === null) {
        name = names.filter(nam => nam.parentId == undefined)
    } else {
        name = names.filter(nam => nam.parentId == parentId)
    }

    const mqttList = name.map(nam => {
        return {
            _id: nam._id,
            parentId: nam.parentId,
            name: nam.name,
            slug: nam.slug,
            user: nam.user,
            children: addPubData(names, nam._id),
            button: nam.button
        }
    })

    return mqttList;
}

exports.getMqtt = async (req, res) => {
    const names = await Mqtt.find({})
        .populate({ path: "user", select: "_id firstname" })
    const mqttList = addPubData(names)
    res.status(200).json({ mqttList })
}