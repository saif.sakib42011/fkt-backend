const shortid = require("shortid");
const slugify = require("slugify");
const Product = require("../models/Product")
const Category = require("../models/Category")
exports.createProduct = async (req, res) => {
    let productPictures = [];
    if (req.files.length > 0) {
        productPictures = req.files.map(file => {
            return {
                img: file.filename
            }
        })
    }
    const { name, price, quantity, description, category } = req.body;
    const product = await Product.create({
        name,
        slug: slugify(name),
        price,
        quantity,
        description,
        productPictures,
        category,
        createdBy: req.user._id,
    })

    res.status(201).json({ product })
}

exports.getProductBySlug = async (req, res) => {
    const { slug } = req.params;
    const category = await Category.findOne({ slug })
        .select("_id");


    if (category) {
        const products = await Product.find({ category: category._id })

        try {
            if (products.length > 0) {
                res.status(200).json({
                    products,
                    productsByPrice: {
                        under5k: products.filter(prod => prod.price <= 5000),
                        under10k: products.filter(prod => prod.price > 5000 && prod.price <= 10000),
                        under15k: products.filter(prod => prod.price > 10000 && prod.price <= 15000),
                        under20k: products.filter(prod => prod.price > 15000 && prod.price <= 20000),
                        under30k: products.filter(prod => prod.price > 20000 && prod.price <= 30000)
                    }

                })
            }
        } catch (error) {
            res.status(400).json({ error: error.message })
        }
    }


}