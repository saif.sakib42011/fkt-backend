
const Category=require("../../models/Category")
const Product=require("../../models/Product")
function addCategories(categories, parentId = null) {
    const categList = []
    let category;
    if (parentId === null) {
        category = categories.filter(cat => cat.parentId == undefined)
    } else {
        category = categories.filter(cat => cat.parentId == parentId)
    }


    // for (let cate of category){
    //   categList.push({
    //     _id:cate._id,
    //     name:cate.name,
    //     slug:cate.name,
    //     children:addCategories(categories,cate._id)
    //    })
    // }
    // return categList;

    const categoryList = category.map(cate => {
        return {
            _id: cate._id,
            parentId:cate.parentId,
            name: cate.name,
            slug: cate.name,
            children: addCategories(categories, cate._id)
        }
    })
    return categoryList;

}
exports.initialdata=async(req,res)=>{
    const categories = await Category.find({});
    const products = await Product.find({})
                                   .select("_id name price quantity slug description productPictures category")
                                   .populate({path:"category",select:"_id name"});
    res.status(200).json({categories:addCategories(categories),products});
}