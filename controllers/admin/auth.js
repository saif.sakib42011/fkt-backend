const User = require("../../models/User");
const shortid=require("shortid")

exports.register = async (req, res) => {
    try {
        const { firstname, lastname, email, password } = req.body;
        const user = await User.create({
            firstname,
            lastname,
            email,
            password,
            username: shortid.generate(),
            role: "admin"
        });

        res.status(201).json({ success: true, user })
    } catch (error) {
        res.status(500).json({ success: false, error: "admin email already exists" })
    }

}


exports.login = async (req, res) => {
    try {
        const { email, password } = req.body;
        const user = await User.findOne({ email }).select("+password");
        if (!user) {
            res.status(401).json({ success: "false", error: "invalid credentials" });
        }

        const isMatch = await user.matchPassword(password);
        if (!isMatch) {
            res.status(404).json({ success: "false", error: "pls provide valid email and password" });
        }

        if (user.role === "admin") {
            signedinToken(200, res, user);
        }

        

    } catch (error) {
        res.status(401).json({ success: "false", error: "invalid credentials" });
    }
}


const signedinToken = (statusCode, res, user) => {
    const token = user.getSignedToken();
    res.cookie('token',token,{expiresIn:'1hr'})
    return res.status(statusCode).json(
        { token, user })
}


exports.logout = async (req, res) => {
    res.clearCookie('token');
    res.status(200).json({message:"signout successfully"})
}

exports.getUsers = async(req,res) => {
    const users=await User.find({})
    const onlyusers=users.filter(user=>user.role==="user")
    res.status(200).json({onlyusers})
}

