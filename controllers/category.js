const Category = require("../models/Category")
const slugify = require("slugify")
const shortid = require("shortid")
exports.createCategory = async (req, res) => {

    try {
        let categoryImage;
        if (req.file) {
            categoryImage = process.env.API + "/public/" + req.file.filename
        }
        const { name } = req.body;
        const category = await Category.create({
            name: name,
            slug: `${slugify(name)}-${shortid.generate()}}`,
            categoryImage,
            parentId: req.body.parentId ? req.body.parentId : undefined
        })

        res.status(201).json({ category })


    } catch (error) {
        res.status(400).json({ error: error.message })
    }
}


function addCategories(categories, parentId = null) {
    const categList = []
    let category;
    if (parentId === null) {
        category = categories.filter(cat => cat.parentId == "")
    } else {
        category = categories.filter(cat => cat.parentId == parentId)
    }




    const categoryList = category.map(cate => {
        return {
            _id: cate._id,
            parentId: cate.parentId,
            name: cate.name,
            slug: cate.name,
            children: addCategories(categories, cate._id)
        }
    })
    return categoryList;

}
exports.getCategory = async (req, res) => {
    const categories = await Category.find({});
    const categoryList = await addCategories(categories)
    res.status(200).json({ categoryList })
}

exports.updateCategory = async (req, res) => {
    const { _id, name, parentId, type } = req.body;
    const updateCategories = []
    if (name instanceof Array) {
        for (let i = 0; i < name.length; i++) {
            // const element = array[i];
            const category = {
                name: name[i],
                type: type[i],
                // parentId: parentId !== "" && parentId[i] 
            }
            if (parentId[i] !== "") {
                category.parentId = parentId[i]
            }
            const updateCategory = await Category.findOneAndUpdate({ _id: _id[i] }, category, { new: true })
            updateCategories.push(updateCategory)
        }

        res.status(201).json({updateCategories})
    } else {
        const category = {
            name,
            type,
            // parentId:parentId !== "" && parentId
        }
        if (parentId !== "") {
            category.parentId = parentId
        }
        const updateCategory = await Category.findOneAndUpdate({ _id }, category, { new: true });
        
        res.status(201).json({updateCategory})
    }

}

exports.deleteCategory = async (req, res) => {
    const {id} = req.body.payload;
    const deletedCategories=[]
    for (let i = 0; i < id.length; i++) {
        const deletedCategory=await Category.findOneAndDelete({_id:id[i].id})
        deletedCategories.push(deletedCategory)
    }
    
    deletedCategories.length === id.length ?
    res.status(201).json({message:"yes"}):
    res.status(201).json({message:"no"})

}

