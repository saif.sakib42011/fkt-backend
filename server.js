// add .dotenv: create config.env in root folder(APP) and 
// write the codes(below) on the top of server.js file:
require("dotenv").config({ path: "config.env" });

//connect to db
const { connectDb } = require("./config/db");
connectDb();

//express init
const express = require("express");
const bodyParser= require("body-parser")
const app = express();

//middleware
app.use(express.json());
app.use(express.urlencoded({extended: false}));
// app.use(bodyParser.json());

const { static } = require("express");
const path = require("path")
app.use('/public', express.static(path.join(__dirname, 'uploads')))
//cors
const cors = require("cors");
app.use(cors())
//routes
app.use("/api/auth", require('./routes/admin/auth'))
app.use("/api/auth", require('./routes/auth'))

app.use("/api/category", require('./routes/category'))

app.use("/api/product", require("./routes/product"))

app.use("/api/cart", require("./routes/cart"))

app.use("/api", require("./routes/admin/initialData"));

app.use("/api/mqtt", require('./routes/mqtt'));

//listen to port
const PORT = 5000;

app.listen(PORT || process.env.PORT, () => {
    console.log(`server running successfully on ${process.env.PORT}`);
})


