const mongoose = require("mongoose");
const MqttSchema =new mongoose.Schema({
    name: {
        type: String,
        trim: true,
        required: true
    },
    slug: {
        type: String,
        unique: true,
        required: true
    },
    parentId: {
        type: String,
    },
    user: { type: mongoose.Schema.Types.ObjectId, ref: 'User',
    required: true 
    },
    button: {
        type: Boolean
    },
})

const Mqtt = mongoose.model("Mqtt", MqttSchema)
module.exports = Mqtt