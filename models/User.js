const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const jsonwebtoken = require("jsonwebtoken");
const UserSchema = new mongoose.Schema({
    firstname: {
        type: String,
        trim: true,
        required: true,
        min: 3,
        max: 25
    },
    lastname: {
        type: String,
        trim: true,
        required: true,
        min: 3,
        max: 25
    },
    username: {
        type: String,
        trim: true,
        required: true,
        index: true,
        unique: true,
    },
    email: {
        type: String,
        required: [true, "please provide an email"],
        match: [
            /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
            "please provide a valid email"
        ],
        unique: true,
        lowercase: true,
    },
    password: {
        type: String,
        minlength: 6,
        required: [true, "please provide a password"],
        select: false
    },

    role: {
        type: String,
        enum: ['user', 'admin'],
        default: 'user'
    },
    contactnumber: { type: String },
    profilepicture: { type: String },

}, { timestamps: true });


UserSchema.pre("save", async function (next) {
    if (!this.isModified("password")) {
        next()
    }
    const salt = await bcrypt.genSalt(10);
    this.password = await bcrypt.hash(this.password, salt);
    next()
})

UserSchema.methods.matchPassword = async function (password) {
    return await bcrypt.compare(password, this.password);
}

UserSchema.methods.getSignedToken = function () {
    // require("crypto").randomBytes(35).toString("hex")
    return jsonwebtoken.sign({ id: this._id, role: this.role, email: this._email }, process.env.JWT_SECRET, { expiresIn: process.env.JWT_EXPIRE })
}

const User = mongoose.model("User", UserSchema);
module.exports = User;