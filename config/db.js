const mongoose = require("mongoose")

exports.connectDb = async () => {
    await mongoose.connect(
        process.env.MONGO_URI,
        {
            useNewUrlParser: true,
            useCreateIndex: true,
            useUnifiedTopology: true,
            useFindAndModify: false
        }
    )

    console.log("database is connected");
}