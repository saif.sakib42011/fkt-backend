const User=require("../models/User")
const jsonwebtoken = require("jsonwebtoken");

//export and protect function to safeguard protected data

exports.protect = async (req, res, next) => {
   
    
    try {
        let token;
        if (req.headers.authorization && req.headers.authorization.startsWith("Bearer")) {
            token = req.headers.authorization.split(" ")[1]
        }
        if (!token) {
            res.status(401).json({ success: false, error: "not authorized to access this route" })
        }
        const decoded = jsonwebtoken.verify(token, process.env.JWT_SECRET);
        const user = await User.findById(decoded.id)
        
        if (!user) {
            res.status(404).json({ success: false, error: "no user found with this id" })
        }

        req.user = user;
        next();

    } catch (error) {
        res.status(401).json({ success: false, error: "not authorized to access this route" })
    }

}


exports.adminMiddleware = (req, res , next) => {
    req.user.role === 'admin' ? next() : res.status(400).json({ error: "Access denied" })
}

exports.userMiddleware = (req, res , next) => {
    req.user.role === 'user' ? next() : res.status(400).json({ error: "Access denied" })
}