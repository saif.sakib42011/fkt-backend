const express=require("express");
const router=express.Router();
const {createMqtt,getMqtt}=require("../controllers/mqtt");
const { protect, adminMiddleware } = require("../middleware/private");
const multer = require("multer");
const shortid = require("shortid")



const path = require("path")
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, path.join(path.dirname(__dirname), 'uploads'))
    },
    filename: function (req, file, cb) {
        cb(null, shortid.generate() + '-' + file.originalname)
    }
})
router.route('/createmqtt').post(protect,adminMiddleware,createMqtt)
router.route('/get').get(getMqtt)

module.exports=router