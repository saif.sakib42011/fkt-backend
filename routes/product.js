const express = require("express");

const multer = require("multer");
const shortid = require("shortid")
const router = express.Router();
const path = require("path")
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, path.join(path.dirname(__dirname), 'uploads'))
    },
    filename: function (req, file, cb) {
        cb(null, shortid.generate() + '-' + file.originalname)
    }
})

var upload = multer({ storage: storage })

const { protect, adminMiddleware } = require("../middleware/private");
const { createProduct,getProductBySlug } = require('../controllers/product')

router.route("/create").post(protect, adminMiddleware, upload.array("productPictures"), createProduct)
router.route("/:slug").get(getProductBySlug)
module.exports = router;