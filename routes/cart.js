const express=require("express");
const router=express.Router();

const {createCart}=require('../controllers/cart');
const {protect,userMiddleware}=require('../middleware/private')

router.route('/create').post(protect,userMiddleware,createCart)

module.exports=router