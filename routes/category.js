const express=require("express");
const router=express.Router();
const multer = require("multer");
const shortid = require("shortid")



const path = require("path")
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, path.join(path.dirname(__dirname), 'uploads'))
    },
    filename: function (req, file, cb) {
        cb(null, shortid.generate() + '-' + file.originalname)
    }
})

var upload = multer({ storage: storage })
const {protect,adminMiddleware}=require("../middleware/private")
const {createCategory,getCategory,updateCategory, deleteCategory}=require("../controllers/category");
router.route("/create").post(protect,adminMiddleware,upload.single("categoryImage"),createCategory)
router.route("/get").get(getCategory)
router.route("/update").post(protect,adminMiddleware,upload.array("categoryImage"),updateCategory)
router.route("/delete").post(deleteCategory)

module.exports=router;