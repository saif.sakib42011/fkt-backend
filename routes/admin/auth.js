const express = require("express");
const router = express.Router();
const { register, login, logout, getUsers} = require("../../controllers/admin/auth");
const {adminMiddleware,protect } = require("../../middleware/private");

 router.route("/admin/register").post(register)
router.route("/admin/login").post(login)
router.route("/admin/logout").post(logout)
router.route("/admin/getusers").get(protect,adminMiddleware,getUsers)

module.exports = router;