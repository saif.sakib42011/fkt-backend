const express = require("express");
const router = express.Router();
const { initialdata } = require("../../controllers/admin/initialdata");
// const { protecta,adminMiddleware } = require("../../middleware/private");

router.route("/initialdata").post(initialdata)

module.exports = router;